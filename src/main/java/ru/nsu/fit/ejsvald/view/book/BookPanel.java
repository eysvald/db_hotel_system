package ru.nsu.fit.ejsvald.view.book;

import ru.nsu.fit.ejsvald.navigation.Navigator;

import javax.swing.*;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Stack;

public class BookPanel extends JPanel {
    private JTextField bookId;
    private JTextField company;
    private JTextField guest;
    private JComboBox<Integer> hotelClass;
    private JTextField floor;
    private JTextField roomsNum;
    private JTextField guestsNum;
    private JTextField entryDate;
    private JTextField departureDate;

    public BookPanel(Navigator navigator,
                     Integer id, Integer companyId, Integer guestId, Integer hotelClass, Integer floor,
                     Integer roomsNum, Integer guestsNum, String entryDate, String departureDate) throws ParseException {
        setLayout(new GridBagLayout());
        int textFieldColumnNum = 10;
        bookId = new JTextField(textFieldColumnNum);
        bookId.setEditable(false);
        company = new JTextField(textFieldColumnNum);
        guest = new JTextField(textFieldColumnNum);
        Integer[] classList = navigator.getMainFrame().getDbEx().getBuildingClasses().toArray(Integer[]::new);
        this.hotelClass = new JComboBox<>(classList);
        this.hotelClass.setPreferredSize(new Dimension(85, 20));
        this.floor = new JTextField(textFieldColumnNum);
        this.roomsNum = new JTextField(textFieldColumnNum);
        this.guestsNum = new JTextField(textFieldColumnNum);
        this.entryDate = new JTextField(textFieldColumnNum);
        this.departureDate = new JTextField(textFieldColumnNum);

        initPanel(id, companyId, guestId, hotelClass, floor, roomsNum, guestsNum, entryDate, departureDate);
    }

    private void initPanel(Integer id, Integer companyId, Integer guestId, Integer hotelClass, Integer floor,
                           Integer roomsNum, Integer guestsNum, String entryDate, String departureDate) throws ParseException {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("id: "), c);

        c.gridx = 1;
        bookId.setText(id.toString());
        add(bookId, c);

        c.gridx = 0;
        c.gridy = 1;
        if (companyId == null && guestId != null) {
            add(new JLabel("guest id: "), c);
            c.gridx = 1;
            guest.setText(guestId.toString());
            add(guest, c);
        } else if (companyId != null && guestId == null) {
            add(new JLabel("company id: "), c);
            c.gridx = 1;
            company.setText(companyId.toString());
            add(company, c);
        }

        c.gridx = 0;
        c.gridy = 2;
        add(new JLabel("hotel class: "), c);

        c.gridx = 1;
        this.hotelClass.setSelectedItem(hotelClass.toString());
        add(this.hotelClass, c);

        c.gridx = 0;
        c.gridy = 3;
        add(new JLabel("floor: "), c);

        c.gridx = 1;
        this.floor.setText(floor.toString());
        add(this.floor, c);

        c.gridx = 0;
        c.gridy = 4;
        add(new JLabel("rooms number: "), c);

        c.gridx = 1;
        this.roomsNum.setText(roomsNum.toString());
        add(this.roomsNum, c);

        c.gridx = 0;
        c.gridy = 5;
        add(new JLabel("guests number: "), c);

        c.gridx = 1;
        this.guestsNum.setText(guestsNum.toString());
        add(this.guestsNum, c);

        c.gridx = 0;
        c.gridy = 6;
        add(new JLabel("entry date: "), c);

        c.gridx = 1;
        this.entryDate.setText(convertDateStampToDate(entryDate));
        add(this.entryDate, c);

        c.gridx = 0;
        c.gridy = 7;
        add(new JLabel("departure date: "), c);

        c.gridx = 1;
        this.departureDate.setText(convertDateStampToDate(departureDate));
        add(this.departureDate, c);
    }


    private String convertDateStampToDate(String date) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(date));
    }

    public void createVisit() throws ParseException {
        Date checkinDate = new SimpleDateFormat("dd/MM/yyyy").parse(entryDate.getText());
        Date checkoutDate = new SimpleDateFormat("dd/MM/yyyy").parse(departureDate.getText());
    }

    public void save() throws Exception {
        int companyIdToSave, guestIdToSave, hotelClassToSave;
        if (company.getText().isEmpty()) {
            if (guest.getText().isEmpty()) {
                throw new Exception();
            }
            companyIdToSave = Integer.parseInt(company.getText());
        } else if (guest.getText().isEmpty()) {
            guestIdToSave = Integer.parseInt(guest.getText());
        }
        hotelClassToSave = (int) hotelClass.getSelectedItem();
        this.floor = new JTextField();
        this.roomsNum = new JTextField();
        this.guestsNum = new JTextField();
        this.entryDate = new JTextField();
        this.departureDate = new JTextField();
    }
}