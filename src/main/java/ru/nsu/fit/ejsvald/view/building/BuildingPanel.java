package ru.nsu.fit.ejsvald.view.building;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.DbEx;

import javax.swing.*;
import java.awt.*;
import java.util.Stack;

public class BuildingPanel extends JPanel {
    private DbEx dbEx;
    private int rowsPerPage = 10;
    private JTextField hotelId;
    private JTextField hotelClass;
    private JTextField floorsNum;
    private JTextField roomsNum;
    private JTextField roomsPerFloor;
    private JScrollPane spTable;

    public BuildingPanel(Navigator navigator, int hotelId, int hotelClass, int floorsNum, int roomsNum) {
        super(new GridBagLayout());
        dbEx = navigator.getMainFrame().getDbEx();
        this.hotelId = new JTextField(String.valueOf(hotelId), 10);
        this.hotelClass = new JTextField(String.valueOf(hotelClass), 10);
        this.floorsNum = new JTextField(String.valueOf(floorsNum), 10);
        this.roomsNum = new JTextField(String.valueOf(roomsNum), 10);
        roomsPerFloor = new JTextField(String.valueOf(roomsNum / floorsNum), 10);
        roomsPerFloor.setEditable(false);
        JTable table = new JTable(dbEx.getBuildingExpenses(0, rowsPerPage));
        spTable = new JScrollPane(table);
        fillTable();
    }

    public BuildingPanel() {
        super(new GridBagLayout());
        this.hotelId = new JTextField();
        this.hotelClass = new JTextField();
        this.floorsNum = new JTextField();
        this.roomsNum = new JTextField();

        roomsPerFloor = new JTextField();
        roomsPerFloor.setEditable(false);
        fillTable();
    }

    private void fillTable() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("id: "), c);

        c.gridx = 1;
        add(this.hotelId, c);

        c.gridx = 0;
        c.gridy = 1;
        add(new JLabel("hotel class: "), c);

        c.gridx = 1;
        add(this.hotelClass, c);

        c.gridx = 0;
        c.gridy = 2;
        add(new JLabel("floors number: "), c);

        c.gridx = 1;
        add(this.floorsNum, c);

        c.gridx = 0;
        c.gridy = 3;
        add(new JLabel("rooms number: "), c);

        c.gridx = 1;
        add(this.roomsNum, c);

        c.gridx = 0;
        c.gridy = 4;
        add(new JLabel("rooms per floor: "), c);

        c.gridx = 1;
        add(roomsPerFloor, c);

        c.gridx = 0;
        c.gridy = 5;
        add(new JLabel("building expenses: "), c);

        c.gridy = 6;
        c.gridwidth = 2;
        add(spTable, c);
    }
}
