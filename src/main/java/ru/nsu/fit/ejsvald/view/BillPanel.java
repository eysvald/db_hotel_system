package ru.nsu.fit.ejsvald.view;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.building.BuildingPanel;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;

public class BillPanel extends JPanel {
    private DbEx dbEx;
    private JTable table;

    public BillPanel(Navigator navigator, int id) {
        setMinimumSize(new Dimension(600, 500));
        setPreferredSize(new Dimension(1000, 500));
        setLayout(new GridBagLayout());
        dbEx = navigator.getMainFrame().getDbEx();
        table = new JTable(dbEx.getBillById(id));
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
    }
}
