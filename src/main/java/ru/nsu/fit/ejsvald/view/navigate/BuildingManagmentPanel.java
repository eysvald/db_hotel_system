package ru.nsu.fit.ejsvald.view.navigate;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.ServiceListPanel;
import ru.nsu.fit.ejsvald.view.building.BuildingListPanel;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class BuildingManagmentPanel extends JPanel {
    private final Insets buttonInsets = new Insets(0, 0, 15, 0);
    private final LinkedList<JButton> buttons = new LinkedList<>();

    public BuildingManagmentPanel(Navigator navigator) {
        setPreferredSize(new Dimension(1000, 500));
        setLayout(new GridBagLayout());
        JButton buildingList = new JButton("Building list");
        buildingList.addActionListener(e -> {
            BuildingListPanel buildingListPanel = new BuildingListPanel(navigator);
            navigator.goForward(buildingListPanel);
        });
        buttons.add(buildingList);

        JButton serviceList = new JButton("Service list");
        serviceList.addActionListener(e -> {
            ServiceListPanel serviceListPanel = new ServiceListPanel(navigator);
            navigator.goForward(serviceListPanel);
        });
        buttons.add(serviceList);
        fillPanel();
    }

    private void fillPanel() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.CENTER;
        c.insets = buttonInsets;

        JButton firstButton = buttons.get(0);
        c.weighty = 0.5;
        c.anchor = GridBagConstraints.SOUTH;
        add(firstButton, c);
        for (int i = 1; i < buttons.size() - 1; i++) {
            c.weighty = 0;
            c.gridy++;
            JButton b = buttons.get(i);
            add(b, c);
        }
        JButton lastButton = buttons.get(buttons.size() - 1);
        c.anchor = GridBagConstraints.NORTH;
        c.weighty = 0.5;
        c.gridy++;
        add(lastButton, c);
    }
}
