package ru.nsu.fit.ejsvald.view.navigate;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.guest.GuestListPanel;
import ru.nsu.fit.ejsvald.view.visit.VisitListPanel;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class GuestManagmentPanel extends JPanel {
    private final Insets buttonInsets = new Insets(0, 0, 15, 0);
    private final LinkedList<JButton> buttons = new LinkedList<>();

    public GuestManagmentPanel(Navigator navigator) {
        setPreferredSize(new Dimension(1000, 500));
        setLayout(new GridBagLayout());
        JButton guestsList = new JButton("Guests list");
        guestsList.addActionListener(e -> {
            GuestListPanel guestListPanel = new GuestListPanel(navigator);
            navigator.goForward(guestListPanel);
        });
        buttons.add(guestsList);

        JButton visitsList = new JButton("Visits list");
        visitsList.addActionListener(e -> {
            VisitListPanel visitListPanel = new VisitListPanel(navigator);
            navigator.goForward(visitListPanel);
        });
        buttons.add(visitsList);
        fillPanel();
    }

    private void fillPanel() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.CENTER;
        c.insets = buttonInsets;

        JButton firstButton = buttons.get(0);
        c.weighty = 0.5;
        c.anchor = GridBagConstraints.SOUTH;
        add(firstButton, c);
        for (int i = 1; i < buttons.size() - 1; i++) {
            c.weighty = 0;
            c.gridy++;
            JButton b = buttons.get(i);
            add(b, c);
        }
        JButton lastButton = buttons.get(buttons.size() - 1);
        c.anchor = GridBagConstraints.NORTH;
        c.weighty = 0.5;
        c.gridy++;
        add(lastButton, c);
    }
}
