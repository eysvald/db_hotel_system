package ru.nsu.fit.ejsvald.view.building;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.DbEx;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;

public class BuildingListPanel extends JPanel {
    private DbEx dbEx;
    private int rowsPerPage = 20;
    private JTable table;

    public BuildingListPanel(Navigator navigator) {
        setMinimumSize(new Dimension(600, 500));
        setPreferredSize(new Dimension(1000, 500));
        setLayout(new GridBagLayout());
        dbEx = navigator.getMainFrame().getDbEx();
        table = new JTable(dbEx.getBuildingList(0, rowsPerPage));
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
        JButton editButton = new JButton("edit");
        editButton.addActionListener(e -> {
            int column = 0;
            int row = table.getSelectedRow();
            int id = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            int hotelClass = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            int floorsNum = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            int roomsNum = ((BigDecimal) table.getModel().getValueAt(row, column)).intValue();
            JPanel buildingPanel = new BuildingPanel(navigator, id, hotelClass, floorsNum, roomsNum);
            navigator.goForward(buildingPanel);
        });
        add(editButton);
    }
}
