package ru.nsu.fit.ejsvald.view;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class DbEx {
    private Connection connection;
    private String userName;
    private String password;

    public DbEx(String address, String userName, String password) throws SQLException {
        this.connection = DriverManager.getConnection(address, userName, password);
        this.userName = userName;
        this.password = password;
    }

    public void updateGuest() {
        String query = "update \"guest\" set \"full_name\" = 'Першин Иван Алексеевич', \"passport_number\" = 1010100100, \"registration_date\" = TO_DATE('2022-03-15', 'yyyy-mm-dd') where \"id\" = 1";
        updateTable(query);
    }

    public TableModel getBillById(int id) {
        String query = "select \"bill\", \"name\", \"bill__service\".price\n" +
                "from \"bill__service\"\n" +
                "         left join \"service\" s on s.\"id\" = \"bill__service\".\"service\"\n" +
                "where \"bill\" = " + id;
        return getTableModel(query);
    }

    public TableModel getBillByVisit(int id) {
        String query = "select \"id\", \"status\", \"bill_date\" from \"bill\" where \"visit\"=" + id;
        return getTableModel(query);
    }

    public TableModel getVisitsByGuest(int guestId) {
        String query = "select \"id\", \"room\", \"check_in_date\", \"check_out_date\", \"book\"\n" +
                "from (select \"visit\" as visit_by_guest from \"guest__visit\" where \"guest\" =" + guestId + ")\n" +
                "         left join \"visit\" on visit_by_guest = \"visit\".\"id\"";
        return getTableModel(query);
    }

    public TableModel getServiceList() {
        String query = "select \"id\", \"name\", \"price\" from \"service\"";
        return getTableModel(query);
    }

    public TableModel getActiveVisitList() {
        String query = "select \"id\", \"room\", \"check_in_date\", \"check_out_date\", \"book\"\n" +
                "from \"visit\"\n" +
                "where \"check_out_date\" is null";
        return getTableModel(query);
    }

    public TableModel getCompaniesBookList() {
        String query = "select \"id\",\n" +
                "       \"company\",\n" +
                "       \"hotel_class\",\n" +
                "       \"floor\",\n" +
                "       \"rooms_number\",\n" +
                "       \"guests_number\",\n" +
                "       \"entry_date\",\n" +
                "       \"departure_date\"\n" +
                "from \"book\"\n" +
                "where \"company\" is not null";
        return getTableModel(query);
    }

    public TableModel getActiveCompaniesBookList() {
        String query = "select \"id\",\n" +
                "       \"company\",\n" +
                "       \"hotel_class\",\n" +
                "       \"floor\",\n" +
                "       \"rooms_number\",\n" +
                "       \"guests_number\",\n" +
                "       \"entry_date\",\n" +
                "       \"departure_date\"\n" +
                "from \"book\"\n" +
                "where \"company\" is not null and \"entry_date\" < SYSDATE";
        return getTableModel(query);
    }

    public TableModel getGuestsBookList() {
        String query = "select \"id\",\n" +
                "       \"guest\",\n" +
                "       \"hotel_class\",\n" +
                "       \"floor\",\n" +
                "       \"rooms_number\",\n" +
                "       \"guests_number\",\n" +
                "       \"entry_date\",\n" +
                "       \"departure_date\"\n" +
                "from \"book\"\n" +
                "where \"guest\" is not null";
        return getTableModel(query);
    }

    public TableModel getActiveGuestsBookList() {
        String query = "select \"id\",\n" +
                "       \"guest\",\n" +
                "       \"hotel_class\",\n" +
                "       \"floor\",\n" +
                "       \"rooms_number\",\n" +
                "       \"guests_number\",\n" +
                "       \"entry_date\",\n" +
                "       \"departure_date\"\n" +
                "from \"book\"\n" +
                "where \"guest\" is not null and \"entry_date\" < SYSDATE";
        return getTableModel(query);
    }

    public TableModel getVisitList() {
        String query = "select \"id\", \"room\", \"check_in_date\", \"check_out_date\", \"book\" from \"visit\"";
        return getTableModel(query);
    }

    public TableModel getGuestList() {
        String query = "select \"id\", \"full_name\", \"passport_number\", \"registration_date\" from \"guest\"";
        return getTableModel(query);
    }

    public TableModel getBuildingList(int pageNum, int rowsPerPage) {
        String query = "select * " +
                "from (select \"building\".*, row_number() over (ORDER BY \"building\".\"id\") line_number from \"building\")\n" +
                "WHERE line_number BETWEEN " + pageNum * rowsPerPage + " AND " + pageNum * rowsPerPage + rowsPerPage + "\n" +
                "ORDER BY line_number";
        return getTableModel(query);
    }

    public TableModel getBuildingExpenses(int pageNum, int rowsPerPage) {
        String query = "select *\n" +
                "from (select \"building_month_expenses\".*, row_number() over (ORDER BY \"building_month_expenses\".\"id\") line_number\n" +
                "      from \"building_month_expenses\")\n" +
                "WHERE line_number BETWEEN " + pageNum * rowsPerPage + " AND " + pageNum * rowsPerPage + rowsPerPage + "\n" +
                "ORDER BY line_number";
        return getTableModel(query);
    }

    public Integer getVisitSumForServices(int visitId, int billStatus) {//todo test
        String query = "select sum(PRICE)\n" +
                "from \"bill\"\n" +
                "         right join \"bill__service\" bs on \"bill\".\"id\" = bs.\"bill\"\n" +
                "where (\"bill\".\"visit\" = " + visitId + " and \"status\" = " + billStatus + ")\n" +
                "group by \"visit\"";
        ResultSet rs;
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            rs = statement.executeQuery();
            return rs.getInt(0);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    public TableModel getRoomById(int id) {//todo test
        String query = "select \"building\", \"room\", \"capacity\", \"cost_per_night\"" +
                "from \"room\" where (\"id\"=" + id + ")";
        return getTableModel(query);
    }

    public List<Integer> getBuildingClasses() {//todo test
        String query = "select distinct \"class\" from \"building\"";
        ResultSet rs;
        List<Integer> list = new LinkedList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            rs = statement.executeQuery();

            while (rs.next()) {
                list.add(rs.getInt(1));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }


    private void updateTable(String query) {
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private TableModel getTableModel(String query) {
        TableModel tableModel;
        try {
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            tableModel = buildTableModel(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
        return tableModel;
    }

    private static DefaultTableModel buildTableModel(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }

        return new DefaultTableModel(data, columnNames);
    }

}
