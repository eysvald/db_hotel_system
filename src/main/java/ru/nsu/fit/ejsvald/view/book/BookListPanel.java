package ru.nsu.fit.ejsvald.view.book;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.DbEx;
import ru.nsu.fit.ejsvald.view.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.text.ParseException;

public class BookListPanel extends JPanel {
    private DbEx dbEx;
    private int rowsPerPage = 20;
    private JTable table;
    private JCheckBox companyBox;
    private JCheckBox guestBox;
    private JCheckBox activeOnlyBox;
    private JScrollPane tableSp;
    private MainFrame mainFrame;
    private Navigator navigator;

    public BookListPanel(Navigator navigator) {
        setMinimumSize(new Dimension(600, 500));
        setPreferredSize(new Dimension(1000, 500));
        setLayout(new GridBagLayout());
        this.navigator = navigator;
        mainFrame = navigator.getMainFrame();
        dbEx = mainFrame.getDbEx();
        initPanel();
    }

    private void initPanel() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 20, 10);
        c.anchor = GridBagConstraints.EAST;
        guestBox = new JCheckBox("guests", true);
        add(guestBox, c);

        c.weightx = 0;
        c.gridx = 1;
        c.anchor = GridBagConstraints.WEST;
        companyBox = new JCheckBox("companies", false);
        add(companyBox, c);

        c.gridx = 2;
        c.anchor = GridBagConstraints.WEST;
        activeOnlyBox = new JCheckBox("active only", true);
        add(activeOnlyBox, c);

        companyBox.addItemListener(e -> {
            JCheckBox cb = (JCheckBox) e.getSource();
            if (cb.isSelected()) {
                if (guestBox != null && guestBox.isSelected()) {
                    guestBox.setSelected(false);
                }
            } else {
                if (guestBox != null && !guestBox.isSelected()) {
                    guestBox.setSelected(true);
                }
            }
            updateTable();
            mainFrame.pack();
            repaint();
        });
        guestBox.addItemListener(e -> {
            JCheckBox cb = (JCheckBox) e.getSource();
            if (cb.isSelected()) {
                if (companyBox != null && companyBox.isSelected()) {
                    companyBox.setSelected(false);
                }
            } else {
                if (companyBox != null && !companyBox.isSelected()) {
                    companyBox.setSelected(true);
                }
            }
            updateTable();
            mainFrame.pack();
            repaint();
        });

        activeOnlyBox.addActionListener(e -> {
            updateTable();
            mainFrame.pack();
            repaint();
        });

        JButton editButton = new JButton("edit");
        editButton.addActionListener(e -> {
            int column = 0;
            int row = table.getSelectedRow();
            int id = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();

            Integer company, guest;
            if (companyBox.isSelected()) {
                company = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
                guest = null;
            } else {
                company = null;
                guest = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            }

            int hotelClass = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            int floor = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            int roomsNum = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            int guestsNum = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            String entryDate = table.getModel().getValueAt(row, column++).toString();
            String departureDate = table.getModel().getValueAt(row, column++).toString();

            try {
                BookPanel bookPanel = new BookPanel(navigator, id, company, guest, hotelClass, floor, roomsNum, guestsNum,
                        entryDate, departureDate);
                navigator.goForward(bookPanel);
            } catch (ParseException parseException) {
                parseException.printStackTrace();
            }

        });
        add(editButton);


        updateTable();
        mainFrame.pack();
        repaint();
    }

    private void updateTable() {
        if (companyBox.isSelected()) {
            if (activeOnlyBox.isSelected()) {
                table = new JTable(dbEx.getActiveCompaniesBookList());
            } else {
                table = new JTable(dbEx.getCompaniesBookList());
            }

        } else {
            if (activeOnlyBox.isSelected()) {
                table = new JTable(dbEx.getActiveGuestsBookList());
            } else {
                table = new JTable(dbEx.getGuestsBookList());
            }
        }
        if (tableSp != null) remove(tableSp);
        tableSp = new JScrollPane(table);
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = 4;
        c.gridx = 0;
        c.gridy = 1;
        add(tableSp, c);
    }

}
