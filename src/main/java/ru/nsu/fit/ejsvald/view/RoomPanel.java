package ru.nsu.fit.ejsvald.view;

import ru.nsu.fit.ejsvald.navigation.Navigator;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RoomPanel extends JPanel {
    private DbEx dbEx;
    private JTextField roomId;
    private JTextField buildingId;
    private JTextField room;
    private JTextField capacity;
    private JTextField costPerNight;

    public RoomPanel(Navigator navigator, Integer id) {
        setLayout(new GridBagLayout());
        dbEx = navigator.getMainFrame().getDbEx();
        int textFieldColumnNum = 15;
        JTable table = new JTable(dbEx.getRoomById(id));
        roomId = new JTextField(textFieldColumnNum);
        buildingId = new JTextField(textFieldColumnNum);
        room = new JTextField(textFieldColumnNum);
        this.capacity = new JTextField(textFieldColumnNum);
        this.costPerNight = new JTextField(textFieldColumnNum);

        int column = 0;
        int row = 0;
        int building = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
        int roomsNum = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
        int capacity = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
        int cost = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();

        initPanel(id, building, roomsNum, capacity, cost);
    }

    private void initPanel(Integer id, Integer building, Integer roomsNum, Integer capacity, Integer cost) {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("id: "), c);

        c.gridx = 1;
        roomId.setText(id.toString());
        add(roomId, c);

        c.gridx = 0;
        c.gridy = 1;
        add(new JLabel("building: "), c);
        c.gridx = 1;
        buildingId.setText(building.toString());
        add(buildingId, c);

        c.gridx = 0;
        c.gridy = 2;
        add(new JLabel("room: "), c);

        c.gridx = 1;
        this.room.setText(roomsNum.toString());
        add(this.room, c);

        c.gridx = 0;
        c.gridy = 3;
        add(new JLabel("capacity: "), c);

        c.gridx = 1;
        this.capacity.setText(capacity.toString());
        add(this.capacity, c);

        c.gridx = 0;
        c.gridy = 4;
        add(new JLabel("cost per night: "), c);

        c.gridx = 1;
        this.costPerNight.setText(cost.toString());
        add(this.costPerNight, c);
    }

    public void save() throws Exception {

    }
}