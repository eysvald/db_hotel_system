package ru.nsu.fit.ejsvald.view.visit;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.DbEx;
import ru.nsu.fit.ejsvald.view.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.text.ParseException;

public class VisitListPanel extends JPanel {
    private DbEx dbEx;
    private int rowsPerPage = 20;
    private JTable table;
    private JCheckBox activeOnlyBox;
    private JScrollPane tableSp;
    private MainFrame mainFrame;

    public VisitListPanel(Navigator navigator) {
        setMinimumSize(new Dimension(600, 500));
        setPreferredSize(new Dimension(1000, 500));
        setLayout(new GridBagLayout());
        mainFrame = navigator.getMainFrame();

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0;
        c.anchor = GridBagConstraints.WEST;
        c.insets = new Insets(0, 0, 20, 10);
        c.anchor = GridBagConstraints.EAST;
        activeOnlyBox = new JCheckBox("active only: ", true);
        add(activeOnlyBox, c);

        activeOnlyBox.addActionListener(e -> {
            updateTable();
            mainFrame.pack();
            repaint();
        });

        dbEx = mainFrame.getDbEx();
        updateTable();
        mainFrame.pack();
        repaint();

        JButton editButton = new JButton("edit");
        editButton.addActionListener(e -> {
            int column = 0;
            int row = table.getSelectedRow();
            int id = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            int room = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            String checkInDate = table.getModel().getValueAt(row, column++).toString();
            String checkOutDate = table.getModel().getValueAt(row, column).toString();
            try {
                VisitPanel visitPanel = new VisitPanel(navigator, id, room, checkInDate, checkOutDate);
                navigator.goForward(visitPanel);
            } catch (ParseException parseException) {
                parseException.printStackTrace();
            }
        });
        add(editButton);
    }

    private void updateTable() {
        if (activeOnlyBox.isSelected()) {
            table = new JTable(dbEx.getActiveVisitList());
        } else {
            table = new JTable(dbEx.getVisitList());
        }

        if (tableSp != null) remove(tableSp);
        tableSp = new JScrollPane(table);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        add(tableSp, c);
    }
}
