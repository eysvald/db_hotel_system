package ru.nsu.fit.ejsvald.view;

import ru.nsu.fit.ejsvald.navigation.Navigator;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;

public class ServiceListPanel extends JPanel {
    private DbEx dbEx;
    private int rowsPerPage = 20;
    private JTable table;

    public ServiceListPanel(Navigator navigator) {
        setMinimumSize(new Dimension(600, 500));
        setPreferredSize(new Dimension(1000, 500));
        setLayout(new GridBagLayout());
        dbEx = navigator.getMainFrame().getDbEx();
        table = new JTable(dbEx.getServiceList());
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
        JButton editButton = new JButton("edit");
        editButton.addActionListener(e -> {

        });
        add(editButton);
    }
}
