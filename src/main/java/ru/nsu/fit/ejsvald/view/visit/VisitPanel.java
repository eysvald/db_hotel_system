package ru.nsu.fit.ejsvald.view.visit;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.BillPanel;
import ru.nsu.fit.ejsvald.view.DbEx;
import ru.nsu.fit.ejsvald.view.MainFrame;
import ru.nsu.fit.ejsvald.view.RoomPanel;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class VisitPanel extends JPanel {
    private DbEx dbEx;
    private MainFrame mainFrame;
    private Navigator navigator;
    private JTextField visitId;
    private JTextField roomField;
    private JTextField checkinDate;
    private JTextField checkoutDate;
    private JScrollPane billsSp;
    private JScrollPane reviewsPs;

    public VisitPanel(Navigator navigator, int id, int room, String checkInDate, String checkOutDate) throws ParseException {
        super(new GridBagLayout());
        this.navigator = navigator;
        mainFrame = navigator.getMainFrame();
        dbEx = mainFrame.getDbEx();
        visitId = new JTextField(15);
        roomField = new JTextField(15);
        checkinDate = new JTextField(15);
        checkoutDate = new JTextField(15);
        initPanel(id, room, checkInDate, checkOutDate);
    }

    private void initPanel(Integer id, Integer room, String checkInDate, String checkOutDate) throws ParseException {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("id: "), c);

        c.gridx = 1;
        visitId.setText(id.toString());
        add(visitId, c);

        c.gridx = 0;
        c.gridy = 1;
        add(new JLabel("room id: "), c);

        c.gridx = 1;
        roomField.setText(room.toString());
        add(roomField, c);

        c.gridx = 0;
        c.gridy = 2;
        add(new JLabel("check in date: "), c);

        c.gridx = 1;
        this.checkinDate.setText(convertDateStampToDate(checkInDate));
        add(this.checkinDate, c);

        c.gridx = 0;
        c.gridy = 3;
        add(new JLabel("check out date: "), c);

        c.gridx = 1;
        this.checkoutDate.setText(convertDateStampToDate(checkOutDate));
        add(this.checkoutDate, c);

        c.gridx = 0;
        c.gridy = 4;
        add(new JLabel("bills: "), c);

        c.gridy = 5;
        c.gridwidth = 2;
        JTable billTable = new JTable(dbEx.getBillByVisit(id));
        billsSp = new JScrollPane(billTable);
        add(billsSp, c);

        c.gridy = 6;
        c.gridx = 0;
        c.gridwidth = 1;
        JButton openRoomButton = new JButton("open room");
        openRoomButton.addActionListener(e -> {
            RoomPanel roomPanel = new RoomPanel(navigator, room);
            navigator.goForward(roomPanel);
        });
        add(openRoomButton, c);

        c.gridy = 6;
        c.gridx = 1;
        JButton openButton = new JButton("open bill");
        openButton.addActionListener(e -> {
            int column = 0;
            int row = billTable.getSelectedRow();
            int billId = ((BigDecimal) billTable.getModel().getValueAt(row, column++)).intValue();

            BillPanel billPanel = new BillPanel(navigator, billId);
            navigator.goForward(billPanel);
        });
        add(openButton, c);
    }


    private String convertDateStampToDate(String date) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(date));
    }

}
