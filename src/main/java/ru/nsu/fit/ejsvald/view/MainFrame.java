package ru.nsu.fit.ejsvald.view;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.navigate.MainScreenPanel;

import javax.swing.*;
import java.awt.*;
import java.util.Stack;

public class MainFrame extends JFrame {
    protected JToolBar toolBar;
    protected MainScreenPanel mainScreenPanel;
    private Navigator navigator;

    private final Stack<JPanel> panelStack = new Stack<>();
    private DbEx dbEx;

    public MainFrame() {
        setLayout(new BorderLayout());
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            dbEx = new DbEx("jdbc:oracle:thin:@//localhost:1521/orcl", "userdb", "password");
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        navigator = new Navigator(this);
        toolBar = new JToolBar("Main toolbar");
        toolBar.setRollover(true);
        JButton back = new JButton("back");
        back.addActionListener(e -> {
            navigator.goBack();
        });
        toolBar.add(back);
        add(toolBar, BorderLayout.NORTH);
        mainScreenPanel = new MainScreenPanel(navigator);
        navigator.goForward(mainScreenPanel);
    }

    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);
    }

    public Stack<JPanel> getPanelStack() {
        return panelStack;
    }

    public DbEx getDbEx() {
        return dbEx;
    }
}
