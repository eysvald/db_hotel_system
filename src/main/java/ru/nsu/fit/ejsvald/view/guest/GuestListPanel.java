package ru.nsu.fit.ejsvald.view.guest;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.DbEx;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.text.ParseException;

public class GuestListPanel extends JPanel {
    private DbEx dbEx;
    private int rowsPerPage = 20;
    private JTable table;

    public GuestListPanel(Navigator navigator) {
        setMinimumSize(new Dimension(600, 500));
        setPreferredSize(new Dimension(1000, 500));
        setLayout(new GridBagLayout());
        dbEx = navigator.getMainFrame().getDbEx();
        table = new JTable(dbEx.getGuestList());
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane);
        JButton editButton = new JButton("edit");
        editButton.addActionListener(e -> {
            int column = 0;
            int row = table.getSelectedRow();
            int id = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            String name = table.getModel().getValueAt(row, column++).toString();
            int passportData = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            String date = table.getModel().getValueAt(row, column).toString();
            try {
                GuestPanel guestPanel = new GuestPanel(navigator, id, name, passportData, date);
                navigator.goForward(guestPanel);
            } catch (ParseException parseException) {
                parseException.printStackTrace();
            }

        });
        add(editButton);
    }
}
