package ru.nsu.fit.ejsvald.view.navigate;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.book.BookListPanel;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;

public class MainScreenPanel extends JPanel {
    private final Insets buttonInsets = new Insets(0, 0, 15, 0);
    private final LinkedList<JButton> buttons = new LinkedList<>();

    public MainScreenPanel(Navigator navigator) {
        setPreferredSize(new Dimension(1000, 500));
        setLayout(new GridBagLayout());
        JButton buildingManageButton = new JButton("Building management");
        buildingManageButton.addActionListener(e -> {
            BuildingManagmentPanel buildingManagmentPanel = new BuildingManagmentPanel(navigator);
            navigator.goForward(buildingManagmentPanel);
        });
        buttons.add(buildingManageButton);

        JButton bookManageButton = new JButton("Book management");
        bookManageButton.addActionListener(e -> {
            BookListPanel bookListPanel = new BookListPanel(navigator);
            navigator.goForward(bookListPanel);
        });
        buttons.add(bookManageButton);

        JButton guestManageButton = new JButton("Guests management");
        guestManageButton.addActionListener(e -> {
            GuestManagmentPanel guestManagmentPanel = new GuestManagmentPanel(navigator);
            navigator.goForward(guestManagmentPanel);
        });
        buttons.add(guestManageButton);
        fillPanel();
    }

    private void fillPanel() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.CENTER;
        c.insets = buttonInsets;

        JButton firstButton = buttons.get(0);
        c.weighty = 0.5;
        c.anchor = GridBagConstraints.SOUTH;
        add(firstButton, c);
        for (int i = 1; i < buttons.size() - 1; i++) {
            c.weighty = 0;
            c.gridy++;
            JButton b = buttons.get(i);
            add(b, c);
        }
        JButton lastButton = buttons.get(buttons.size() - 1);
        c.anchor = GridBagConstraints.NORTH;
        c.weighty = 0.5;
        c.gridy++;
        add(lastButton, c);
    }
}
