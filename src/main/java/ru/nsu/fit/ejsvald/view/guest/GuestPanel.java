package ru.nsu.fit.ejsvald.view.guest;

import ru.nsu.fit.ejsvald.navigation.Navigator;
import ru.nsu.fit.ejsvald.view.DbEx;
import ru.nsu.fit.ejsvald.view.visit.VisitPanel;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class GuestPanel extends JPanel {
    private DbEx dbEx;
    private JTextField guestId;
    private JTextField guestName;
    private JTextField passportNumber;
    private JTextField registrationDate;
    private JScrollPane spTable;
    private Navigator navigator;

    public GuestPanel(Navigator navigator, Integer id, String name, Integer passportNumber, String registrationDate) throws ParseException {
        super(new GridBagLayout());
        this.navigator = navigator;
        dbEx = navigator.getMainFrame().getDbEx();
        guestId = new JTextField(20);
        guestName = new JTextField(20);
        this.passportNumber = new JTextField(20);
        this.registrationDate = new JTextField(20);
        initPanel(id, name, passportNumber, registrationDate);
    }

    public GuestPanel() {
        super(new GridBagLayout());
        guestName = new JTextField(10);
        this.passportNumber = new JTextField(10);
        this.registrationDate = new JTextField(10);
    }

    private void initPanel(Integer id, String name, Integer passportNumber, String date) throws ParseException {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        add(new JLabel("id: "), c);

        c.gridx = 1;
        guestId.setText(id.toString());
        add(guestId, c);

        c.gridx = 0;
        c.gridy = 1;
        add(new JLabel("guest name: "), c);

        c.gridx = 1;
        guestName.setText(name);
        add(guestName, c);

        c.gridx = 0;
        c.gridy = 2;
        add(new JLabel("passport data: "), c);

        c.gridx = 1;
        this.passportNumber.setText(passportNumber.toString());
        add(this.passportNumber, c);

        c.gridx = 0;
        c.gridy = 3;
        add(new JLabel("registration date: "), c);

        c.gridx = 1;
        this.registrationDate.setText(convertDateStampToDate(date));
        add(this.registrationDate, c);

        c.gridx = 0;
        c.gridy = 4;
        add(new JLabel("guests visits: "), c);

        c.gridy = 5;
        c.gridwidth = 2;
        JTable table = new JTable(dbEx.getVisitsByGuest(id));
        spTable = new JScrollPane(table);
        add(spTable, c);

        c.gridy = 6;
        c.gridx = 0;
        JButton openButton = new JButton("open");
        openButton.addActionListener(e -> {
            int column = 0;
            int row = table.getSelectedRow();
            int visitId = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            int room = ((BigDecimal) table.getModel().getValueAt(row, column++)).intValue();
            String checkInDate = table.getModel().getValueAt(row, column++).toString();
            String checkOutDate = table.getModel().getValueAt(row, column).toString();
            try {
                VisitPanel visitPanel = new VisitPanel(navigator, visitId, room, checkInDate, checkOutDate);
                navigator.goForward(visitPanel);
            } catch (ParseException parseException) {
                parseException.printStackTrace();
            }
        });
        add(openButton, c);
    }

    private String convertDateStampToDate(String date) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(date));
    }
}
