package ru.nsu.fit.ejsvald.navigation;

import ru.nsu.fit.ejsvald.view.MainFrame;

import javax.swing.*;
import java.awt.*;
import java.util.Stack;

public class Navigator {
    private Stack<JPanel> panelStack;
    private MainFrame mainFrame;

    public Navigator(MainFrame mainFrame) {
        panelStack = new Stack<>();
        this.mainFrame = mainFrame;
    }

    public void goForward(JPanel nextPanel) {
        if (!panelStack.isEmpty()) {
            mainFrame.remove(panelStack.peek());
        }
        mainFrame.add(nextPanel, BorderLayout.CENTER);
        nextPanel.setVisible(true);
        mainFrame.pack();

        panelStack.push(nextPanel);
    }

    public void goBack() {
        if (panelStack.size() < 2) return;
        mainFrame.remove(panelStack.pop());
        mainFrame.add(panelStack.peek(), BorderLayout.CENTER);

        mainFrame.pack();
        mainFrame.repaint();
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }
}
